import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  height: 60px;
  border: 1px solid green;
  align-items: center;
  justify-content: space-between;
  img {
    margin-left: 16px;
  }
  button {
    margin-right: 16px;
    padding: 8px;
    border-radius: 4px;
    background: none;
    border: 2px solid red;
    transition: background 0.3s;

    font-weight: bold;
    font-size: 1.8rem;

    &:hover {
      background: red;
      color: white;
    }
  }
`;
