import React from 'react';

import { useAuth } from '../../context/AuthContext';
import logo from '../../assets/pegaki-logo.png';
import { Container } from './styles';

const Header: React.FC = ({ children }) => {
  const { signOut } = useAuth();

  return (
    <Container>
      <img src={logo} alt="logo" />
      <button onClick={signOut}>Sair</button>
    </Container>
  );
};

export default Header;
