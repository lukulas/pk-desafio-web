import React from 'react';
import 'react-toastify/dist/ReactToastify.css';

import { AuthProvider } from './context/AuthContext';
import Routes from './routes';
import Global from './styles/global';

const App: React.FC = () => {
  return (
    <AuthProvider>
      <Routes />

      <Global />
    </AuthProvider>
  );
};

export default App;
