import React, { useState } from 'react';
import { toast } from 'react-toastify';
import GoogleMapReact from 'google-map-react';
import axios from 'axios';

import { FaHandPointDown } from 'react-icons/fa';

import api from '../../services/api';
import Header from '../../components/Header';

import {
  Container,
  Wrapper,
  MapContainer,
  ContainerSpot,
  TextSpot,
  ButtonSearch,
  ButtonExport,
} from './styles';

toast.configure();

const apiKey = 'AIzaSyBdGqsEkXra_3AzzZGGLGrxso5FLHbSI-Q';

const SpotComponent = ({ text }: any) => (
  <ContainerSpot>
    <TextSpot>{text}</TextSpot>
    <FaHandPointDown color={'red'} size={22} />
  </ContainerSpot>
);

interface ISpotProps {
  latitude: number;
  longitude: number;
}

const Home: React.FC = () => {
  const [center, setCenter] = useState({
    latitude: -23.533773,
    longitude: -46.62529,
  } as ISpotProps);
  const [zoom, setZoom] = useState(10);
  const [isLoading, setIsLoading] = useState(false);
  const [cep, setCep] = useState('');
  const [localities, setLocalities] = useState([]);

  async function HandleSearchCLickButton() {
    if (!isLoading) {
      setIsLoading(true);

      try {
        const responseApiPegaki = await api.get(`/cep/${cep}`);
        const responseGoogleGeo = await axios.get(
          `https://maps.googleapis.com/maps/api/geocode/json?address=${cep}&key=${apiKey}`,
        );

        const {
          lat,
          lng,
        } = responseGoogleGeo.data.results[0].geometry.location;
        setCenter({ latitude: lat, longitude: lng });
        setLocalities(responseApiPegaki.data);
        console.log(responseApiPegaki);
      } catch (error) {
        toast.error('Sem resultados!');
      } finally {
        setIsLoading(false);
      }
    } else {
      toast.info('Aguarde o carregamento...');
    }
  }

  async function HandlePdfGenerateCLickButton() {
    if (isLoading) {
      toast.error('Aguarde o processo finalizar.');
    } else if (localities.length > 0) {
      setIsLoading(true);
      try {
        const response = await api.post('/files', { localities });
        const fileName = response.data;
        window.open(`http://localhost:3333/files/${fileName}`);
      } catch (error) {
        toast.error('Erro ao gerar a planilha');
      } finally {
        setIsLoading(false);
      }
    } else {
      toast.error(
        'Para executar essa operação é preciso consultar um CEP válido',
      );
    }
  }

  return (
    <>
      <Wrapper>
        <Header />
        <Container>
          <h1>Buscar localidades próximas</h1>
          <div>
            <input
              type="number"
              value={cep}
              onChange={e => setCep(e.target.value)}
              placeholder="Ex: 88099324"
            />
            <ButtonSearch onClick={HandleSearchCLickButton}>
              {isLoading ? 'Carregando...' : 'Pesquisar'}
            </ButtonSearch>
            <ButtonExport onClick={HandlePdfGenerateCLickButton}>
              {isLoading ? 'carregando...' : 'Gerar Planilha'}
            </ButtonExport>
          </div>
        </Container>
        <MapContainer>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: apiKey,
            }}
            center={{
              lat: center && center.latitude,
              lng: center && center.longitude,
            }}
            defaultZoom={zoom}
          >
            {!!localities &&
              localities.map((spot: any) => (
                <SpotComponent
                  key={spot.id}
                  lat={spot.latitude}
                  lng={spot.longitude}
                  text={spot.nome_fantasia}
                />
              ))}
          </GoogleMapReact>
        </MapContainer>
      </Wrapper>
    </>
  );
};

export default Home;
