import styled from 'styled-components';
import { darken } from 'polished';

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;

  display: flex;
  flex-direction: column;
`;

export const MapContainer = styled.div`
  height: 80%;
  width: 80%;
  padding: 20px;

  align-self: center;

  margin-top: 36px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  margin-top: 100px;

  h1 {
  }

  div {
    margin-top: 8px;
    display: flex;
    flex-direction: row;
    input {
      max-width: 300px;
      width: 90%;
      padding: 12px 15px;
      border-radius: 4px;
      border: 1;
    }
  }
`;

export const ButtonSearch = styled.button`
  margin-left: 2px;
  padding: 6px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  border: 1;
  background: green;
  color: white;
  font-weight: bold;
  transition: background 0.3s;

  &:hover {
    background: ${darken(0.1, 'green')};
  }
`;

export const ButtonExport = styled.button`
  margin-left: 2px;
  padding: 6px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  border: 1;
  background: blue;
  color: white;
  font-weight: bold;
  transition: background 0.3s;

  &:hover {
    background: ${darken(0.3, 'blue')};
  }
`;

export const ContainerSpot = styled.div`
  display: flex;
  flex-direction: column;

  align-items: center;
  justify-content: center;
`;

export const TextSpot = styled.p`
  font-weight: bold;
  font-size: 1rem;
  text-align: center;
`;
