import styled from 'styled-components';
import { darken } from 'polished';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100vw;
  height: 100vh;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 400px;
  width: 100%;

  background: #fff;
  padding: 24px;
  border-radius: 4px;
  border: 1px solid black;
  align-items: center;

  img {
    margin-bottom: 24px;
  }

  form {
    display: flex;
    flex-direction: column;

    input {
      border: 1px solid black;
      padding: 12px 15px;
      border-radius: 4px;
      width: 300px;
    }
    span {
      color: red;
    }
    label {
      margin: 8px 0 8px;
      font-weight: bold;
    }
    button {
      padding: 12px 0;
      background: green;
      border-radius: 4px;
      border: 0;
      font-weight: bold;
      color: white;
      margin: 16px 0 40px;
      transition: background 0.3s;
      &:hover {
        background: ${darken(0.1, 'green')};
      }
    }
  }
`;
