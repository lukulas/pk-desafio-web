import React, { useState } from 'react';
import { Form } from '@unform/web';
import { toast } from 'react-toastify';

import logo from '../../assets/pegaki-logo.png';
import { Container, Wrapper } from './styles';
import Input from '../../components/Form/Input';
import { useAuth } from '../../context/AuthContext';

interface ISubmitProps {
  email: string;
  password: string;
}

toast.configure();
const Login: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { signIn } = useAuth();

  async function handleOnSubmit({ email, password }: ISubmitProps) {
    if (!isLoading) {
      setIsLoading(true);
      try {
        await signIn({ email, password });
      } catch (error) {
        toast.error('Usuário/Senha invalidos');
      } finally {
        setIsLoading(false);
      }
    } else {
      toast.info('Aguarde o carregamento...');
    }
  }

  return (
    <Wrapper>
      <Container>
        <img src={logo} alt="logo" />
        <Form onSubmit={handleOnSubmit}>
          <label>SEU E-MAIL</label>
          <Input name="email" type="email" placeholder="exemplo@gmail.com" />
          <label>SUA SENHA</label>
          <Input name="password" type="password" placeholder="********" />
          <button type="submit">Entrar no sistema</button>
        </Form>
      </Container>
    </Wrapper>
  );
};

export default Login;
