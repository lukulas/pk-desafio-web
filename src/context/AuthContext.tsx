import React, { createContext, useCallback, useState, useContext } from 'react';
import api from '../services/api';

interface ICredentials {
  email: string;
  password: string;
}

interface IAuthContextData {
  email: string;
  signIn(credentials: ICredentials): Promise<void>;
  signOut(): void;
}

interface IAuthState {
  token: string;
  email: string;
}

const AuthContext = createContext<IAuthContextData>({} as IAuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<IAuthState>(() => {
    const token = localStorage.getItem('@Pegaki:token');
    const email = localStorage.getItem('@Pegaki:email');

    if (token && email) {
      api.defaults.headers.authorization = `Bearer ${token}`;
      return { token, email };
    }

    return {} as IAuthState;
  });

  const signIn = useCallback(async ({ email, password }: ICredentials) => {
    const response = await api.post('session', {
      email,
      client_secret: password,
    });
    const { token, email: emailAccess } = response.data;

    localStorage.setItem('@Pegaki:token', token);
    localStorage.setItem('@Pegaki:email', emailAccess);

    api.defaults.headers.authorization = `Bearer ${token}`;

    setData({ token, email: emailAccess });
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@Pegaki:token');
    localStorage.removeItem('@Pegaki:email');

    setData({} as IAuthState);
  }, []);

  return (
    <AuthContext.Provider value={{ signIn, signOut, email: data.email }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): IAuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth tem que ser usado com AuthProvider.');
  }

  return context;
}

export { useAuth, AuthProvider };
