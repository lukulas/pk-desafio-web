import React from 'react';

import { BrowserRouter } from 'react-router-dom';

import Home from '../pages/Home';
import Login from '../pages/Login';

import Route from './Route';

const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Login} />
      <Route path="/home" component={Home} isPrivate />
    </BrowserRouter>
  );
};

export default Routes;
